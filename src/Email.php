<?php

namespace MailCare\Parser\PHP;

use MailCare\Parser\Email as EmailContract;
use MailCare\Parser\Address as AddressContract;

class Email implements EmailContract
{
    public function __construct($message)
    {
        $this->message = $message;
    }

    public function getFrom(): AddressContract
    {
        $from = $this->message->getHeader('From');

        return new Address($from->getEmail(), $from->getPersonName());
    }

    public function getTo(): array
    {
        return array_map(function ($address) {
            return new Address($address->getEmail(), $address->getName());
        }, $this->message->getHeader('To')->getParts());
    }

    public function getAttachments(): array
    {
        // var_dump(get_class($this->message->getPart(1)));
        // var_dump($this->message->getPart(1)->getContentType());
        // var_dump($this->message->getTextContent());

        $calendars = array_filter($this->message->getAllParts(), function ($part) {
            return $part->getContentType() === 'text/calendar';
        });
        
        $attachments = array_merge($this->message->getAllAttachmentParts(), $calendars);

        return array_map(function ($attachment) {
            return new Attachment($attachment);
        }, $attachments);
    }

    public function getSubject(): string
    {
        return $this->message->getHeaderValue('Subject');
    }

    public function getText(): string
    {
        $text = $this->message->getTextContent();
        return $text ? $text : '';
    }

    public function getHtml(): string
    {
        $html = $this->message->getHtmlContent();
        return $html ? $html : '';
    }
}