<?php

namespace MailCare\Parser\PHP;

use ZBateson\MailMimeParser\Message;
use MailCare\Parser\Email as EmailContract;
use MailCare\Parser\Parser as ParserContract;

class Parser implements ParserContract
{
    public function parseFromStream($stream): EmailContract
    {
        return new Email(Message::from($stream));
    }

    public function parseFromText(string $text): EmailContract
    {
        return new Email(Message::from($text));
    }

    public function parseFromPath(string $path): EmailContract
    {
        return new Email(Message::from(file_get_contents($path)));
    }
}