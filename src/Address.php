<?php

namespace MailCare\Parser\PHP;

use MailCare\Parser\Address as AddressContract;

class Address implements AddressContract
{
    public function __construct(string $email, ?string $name)
    {
        $this->email = $email;
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getDisplay(): string
    {
        return $this->name ?: $this->email;
    }
}