<?php

namespace MailCare\Parser\PHP;

use MailCare\Parser\Attachment as AttachmentContract;

class Attachment implements AttachmentContract
{
    public function __construct($attachment)
    {
        $this->attachment = $attachment;
    }

    public function save(string $directory): string
    {
        $directory = rtrim($directory, '/');
        $path = "{$directory}/{$this->getFilename()}";

        if (! is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        $this->attachment->saveContent($path);

        return $path;
    }

    public function getFilename(): string
    {
        return $this->attachment->getFilename() ?? 'noname1';
    }
}