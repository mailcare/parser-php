<?php

namespace MailCare\Parser\PHP;

use MailCare\Parser\Tests\ParserTest as BaseTest;
use MailCare\Parser\Parser as ParserContract;

class ParserTest extends BaseTest
{
    public function getParser(): ParserContract
    {
        return new Parser;
    }
}